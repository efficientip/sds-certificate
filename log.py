# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-06-08 18:54:10 alex>

"""
logging startup function to avoid duplicate
"""

import logging

def set_log(lvl):
    """set the log level and format for all the programs"""

    log_format = '%(asctime)-15s [%(levelname)s] %(filename)s:%(lineno)d - %(message)s'
    log_level = logging.ERROR

    if lvl == 'INFO':
        log_level = logging.INFO
    if lvl == 'DEBUG':
        log_level = logging.DEBUG
    if lvl == 'WARNING':
        log_level = logging.WARNING
    if lvl == 'ERROR':
        log_level = logging.ERROR

    logging.basicConfig(format=log_format, level=log_level)
