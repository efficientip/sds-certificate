# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-06-08 20:30:37 alex>

"""
create an archive to be uploaded in SOLIDserver with the certificate and its key
"""

import logging
from zipfile import ZipFile

import log

def main(server):
    """main function"""
    file_name = "eip-cert-{}.zip".format(server)

    try:
        with ZipFile(file_name, 'w') as fzip:
            fzip.write("{}.crt".format(server), "certificate")
            fzip.write("{}-private.key".format(server), "private_key")
    except FileNotFoundError:
        logging.error("certificate files not found")
        exit()

if __name__ == '__main__':
    try:
        import argparse
        PARSER = argparse.ArgumentParser(description='EIP certificate ZIP archive')

        PARSER.add_argument('--log', metavar='DEBUG, INFO, WARNING, ERROR',
                            default='INFO',
                            type=str, help='log level', nargs='?',
                            choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])

        PARSER.add_argument('--name',
                            help='certificate name',
                            default='server', nargs='?')

        ARGS = PARSER.parse_args()

    except ImportError:
        logging.error('parse error - exit')
        exit()

    log.set_log(ARGS.log)

    logging.info("create a ZIP file (eip-cert-%s.zip) with certificate and public key",
                 ARGS.name)

    main(ARGS.name)
