# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-06-08 20:29:38 alex>

"""
create a server certificate to be uploaded to the SOLIDserver
uses a CA created with the create_ca.py command
"""

import logging

from OpenSSL import crypto

import compat
import cert
import meta
import log

def main(server, ca_name, config_file):
    """main function"""
    compat.validate_dependencies_met()

    meta_data = meta.get_meta(config_file, server)

    try:
        file_content = open("{}.crt".format(ca_name), 'r').read()
        maincert = crypto.load_certificate(crypto.FILETYPE_PEM, file_content)

        file_content = open("{}-private.key".format(ca_name), 'r').read()
        mainkey = crypto.load_privatekey(crypto.FILETYPE_PEM, file_content)
    except IOError:
        logging.error("cannot load CA files")
        exit()

    (newcert, _) = cert.create_cert(server, maincert, mainkey, meta=meta_data)

    print("Server Certificate created:")
    cert.display_cert(newcert)


if __name__ == '__main__':
    try:
        import argparse
        PARSER = argparse.ArgumentParser(description='EIP certificate CA creator')

        PARSER.add_argument('--log', metavar='DEBUG, INFO, WARNING, ERROR',
                            default='INFO',
                            type=str, help='log level', nargs='?',
                            choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])

        PARSER.add_argument('--name',
                            help='certificate name',
                            default='server', nargs='?')

        PARSER.add_argument('--ca',
                            help='CA file prefix name',
                            default='ca', nargs='?')

        PARSER.add_argument('--config',
                            help='configuration file',
                            default='eip-certs.conf', nargs='?')

        ARGS = PARSER.parse_args()

    except ImportError:
        logging.error('parse error - exit')
        exit()

    log.set_log(ARGS.log)

    logging.info("create certificate for the %s signed with %s",
                 ARGS.name,
                 ARGS.ca)

    main(ARGS.name, ARGS.ca, ARGS.config)
