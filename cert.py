# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-06-08 19:11:32 alex>

"""manipulate the certificates"""

import time

from OpenSSL import crypto

def create_cert(prefix, sign_cert=None, sign_key=None, serial=0, meta=None):
    """create a certificate, either CA self-signed or signed"""
    if meta is None:
        assert None, 'missing meta data'

    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 2048)  # generate RSA key-pair

    cert = crypto.X509()

    cert.set_pubkey(k)

    cert.get_subject().countryName = meta['countryName']
    cert.get_subject().stateOrProvinceName = meta['stateOrProvinceName']
    cert.get_subject().organizationName = meta['organizationName']
    cert.get_subject().localityName = meta['localityName']
    cert.get_subject().organizationalUnitName = meta['organizationalUnitName']
    cert.get_subject().commonName = meta['commonName']

    cert.set_version(2)

    if serial == 0:
        if sign_cert is None: # cert ca
            serial = int(time.time())
        else: # cert server
            serial = int(time.time()+1)

    cert.set_serial_number(serial)

    # certificate for the server
    if sign_cert is not None:
        extension_list = [
            crypto.X509Extension(type_name=b"basicConstraints",
                                 critical=False,
                                 value=b"CA:false"),

            crypto.X509Extension(type_name=b"keyUsage",
                                 critical=False,
                                 value=b"nonRepudiation, digitalSignature, keyEncipherment"),

            crypto.X509Extension(type_name=b"extendedKeyUsage",
                                 critical=False,
                                 value=b"serverAuth"),

            crypto.X509Extension(type_name=b"authorityKeyIdentifier",
                                 critical=False,
                                 value=b"keyid:always",
                                 issuer=sign_cert),
        ]

        sarray = []
        if 'ip' in meta:
            sarray.append("IP:{}".format(meta['ip']))
        if 'dns' in meta:
            sarray.append("DNS:{}".format(meta['dns']))

        if sarray:
            subject = ",".join(sarray)
            extension_list.append(crypto.X509Extension(type_name=b"subjectAltName",
                                                       critical=False,
                                                       value=subject.encode('ascii')))

        cert.add_extensions(extension_list)
    else:
        # certificate for the root CA
        extension_list = [
            crypto.X509Extension(type_name=b"basicConstraints",
                                 critical=False,
                                 value=b"CA:true"),

            crypto.X509Extension(type_name=b"keyUsage",
                                 critical=True,
                                 value=b"keyCertSign, digitalSignature, keyEncipherment"),

            crypto.X509Extension(type_name=b"subjectKeyIdentifier",
                                 critical=False,
                                 value=b"hash",
                                 subject=cert),

        ]
        cert.add_extensions(extension_list)

    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(int(meta['expiry']) * 365 * 24 * 60 * 60)

    if sign_cert is None:
        cert.set_issuer(cert.get_subject())  # self-sign this certificate
        cert.sign(k, 'sha256')
    else:
        cert.set_issuer(sign_cert.get_subject())
        cert.sign(sign_key, 'sha256')

    open("{0}.crt".format(prefix), 'wt').write(
        crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode('ascii'))

    open("{0}-private.key".format(prefix), 'wt').write(
        crypto.dump_privatekey(crypto.FILETYPE_PEM, pkey=k).decode('ascii'))

    open("{0}-public.key".format(prefix), 'wt').write(
        crypto.dump_publickey(crypto.FILETYPE_PEM, pkey=k).decode('ascii'))

    return([cert, k])

def display_cert(cert):
    """display the content of the certificate on stdin"""
    print(" Data:")
    print("   version: {}".format(cert.get_version()+1))
    print("   serial : {}".format(cert.get_serial_number()))

    iss_comp = cert.get_issuer().get_components()
    carray = []
    for comp in iss_comp:
        carray.append("{}={}".format(comp[0].decode('ascii'),
                                     comp[1].decode('ascii')))
    print(" Issuer : {}".format(", ".join(carray)))

    iss_comp = cert.get_subject().get_components()
    carray = []
    for comp in iss_comp:
        carray.append("{}={}".format(comp[0].decode('ascii'),
                                     comp[1].decode('ascii')))
    print(" Subject: {}".format(", ".join(carray)))

    isvalid = "valid"
    if cert.has_expired():
        isvalid = "expired"

    print(" Validity: {}".format(isvalid))
    print("   not before: {}".format(cert.get_notBefore().decode('ascii')))
    print("   not after : {}".format(cert.get_notAfter().decode('ascii')))

    print(" Public Key info:")
    k = cert.get_pubkey()
    ktype = ""
    if k.type() == crypto.TYPE_RSA:
        ktype = "RSA"
    else:
        ktype = "DSA"
    print("   {} {} bits".format(ktype, k.bits()))

    n_ext = cert.get_extension_count()
    if n_ext > 0:
        print(" Extensions:")
        for i in range(n_ext):
            extension = cert.get_extension(i)
            print("   {} = {}".format(extension.get_short_name().decode('ascii'),
                                      extension))


    print(" Signature:")
    print("   algo: {}".format(cert.get_signature_algorithm().decode('ascii')))
