# SOLIDserver-Certificate

Python commands to create CA and certificates for SOLIDserver HTTPS and API connections

## configuration file

The file `eip-certs.conf` contains information for each certificate (CA and servers). It is composed of sections using the name of the certificate (used in `--name` and `--ca` options). Each section can contain traditionnal information in a certificate like:
```
  organizationName = EfficientIP
  organizationalUnitName = EfficientIP PS
  countryName = FR
  stateOrProvinceName = IdF
  localityName = Paris
  commonName = EfficientIP Demo CA
  expiry = 3
```

The server section can also use additionnal fields for server short name or ip if not named:
```
  ip = 192.168.56.254
  dns = lab01
```

## create a self-signed CA

```
python create_ca.py
```

This command creates the certificate file `ca.crt` and key files `ca-private.key` and `ca-public.key`. The certificate is required for browser or certificate store addition to avoid warning messages. It can also be used for API certificate validation.

## create a server certificate

```
python create_server.py
```

This creates a server certificate signed by the CA.

## create the SOLIDserver archive

```
python create_archive.py
```

This command creates the archive to be uploaded in SOLIDserver, named by default `eip-cert-server.zip`

