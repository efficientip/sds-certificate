# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-06-08 18:54:58 alex>

"""
read the meta-data from the configuration file
"""

import configparser
import logging

def get_meta(config_file, section):
    """returns all meta data understood from the configuration file"""
    config = configparser.RawConfigParser(allow_no_value=True)
    config.read(config_file)

    meta = {}

    if section in config.sections():
        for item in ['organizationName',
                     'organizationalUnitName',
                     'countryName',
                     'stateOrProvinceName',
                     'localityName',
                     'commonName']:
            try:
                meta[item] = config.get(section, item)
            except configparser.NoOptionError:
                logging.error("missing %s in the configuration file",
                              item)
                exit()

        meta['expiry'] = config.get(section, 'expiry', fallback='5')

        if config.has_option(section, 'ip'):
            meta['ip'] = config.get(section, 'ip')
        if config.has_option(section, 'dns'):
            meta['dns'] = config.get(section, 'dns')
    else:
        logging.error("missing section %s in configuration file", section)
        return None

    return meta
