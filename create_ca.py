# -*- Mode: Python; python-indent-offset: 4 -*-
#
# Time-stamp: <2019-06-08 20:22:53 alex>

"""
create a Certificate Authority for the SOLIDserver certificates, if needed
"""

import logging

import compat
import cert
import meta
import log

def main(name, config_file):
    """main function"""
    meta_data = None
    meta_data = meta.get_meta(config_file, name)

    if meta_data is None:
        logging.error("need configuration file, see --config")
        exit()

    compat.validate_dependencies_met()
    (maincert, _) = cert.create_cert(name, meta=meta_data)

    print("Certificate Authority created:")
    cert.display_cert(maincert)

if __name__ == '__main__':
    try:
        import argparse
        PARSER = argparse.ArgumentParser(description='EIP certificate CA creator')

        PARSER.add_argument('--log',
                            metavar='DEBUG, INFO, WARNING, ERROR',
                            default='ERROR',
                            type=str, help='log level', nargs='?',
                            choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])

        PARSER.add_argument('--config',
                            help='configuration file',
                            default='eip-certs.conf', nargs='?')

        PARSER.add_argument('--name',
                            help='CA certificate name',
                            default='ca', nargs='?')

        ARGS = PARSER.parse_args()
    except ImportError:
        logging.error('parse error - exit')
        exit()

    log.set_log(ARGS.log)

    logging.info("create certificate for the CA: %s",
                 ARGS.name)

    main(ARGS.name, ARGS.config)
